package green3rd.taxi.vber;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import green3rd.taxi.vber.sharedClass.CardAdapter;
import green3rd.taxi.vber.tabFragment.FavFragment;
import green3rd.taxi.vber.tabFragment.OnFavSelectedListener;
import green3rd.taxi.vber.tabFragment.PlacesFragment;

import static green3rd.taxi.vber.sharedClass.FileHandler.removeFromFile;
import static green3rd.taxi.vber.sharedClass.FileHandler.writeToFile;


public class MainActivity extends AppCompatActivity implements OnFavSelectedListener {

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
//        myToolbar.setTitleTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorTextHeader));
//        myToolbar.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        myToolbar.setTitle("");
        setSupportActionBar(myToolbar);
        //Another way to hide the title.
        //getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Tab
        AppSectionsPagerAdapter mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.pager_main);
        mViewPager.setAdapter(mAppSectionsPagerAdapter);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs_main);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setTabTextColors(
                ContextCompat.getColor(getApplicationContext(), R.color.colorTextHeader),//Normal Color
                ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));//Selected Color

        //ImageLoader
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
        .build();
        ImageLoader.getInstance().init(config);

    }

    @Override
    public void addToFavouriteTab(String name, String address, String url, String icon, String id,LatLng latlng){
        View favTab = mViewPager.getChildAt(1);//Fav Tab
        if(favTab != null){
            RecyclerView rcyView = (RecyclerView)favTab.findViewById(R.id.recyclerView_favPlace);
            CardAdapter adapter = (CardAdapter)rcyView.getAdapter();
            if(adapter.addCard(name, address, url, icon, id,latlng)) {
                Toast.makeText(this, "Added to Favourites.",
                        Toast.LENGTH_LONG).show();


                //Save to file
                writeToFile(name, address, url, icon, id,latlng,getApplicationContext());


            }else{
                Toast.makeText(this, "Added.",
                        Toast.LENGTH_LONG).show();
            }


        }

    }

    @Override
    public void removeFromFavouriteTab(String id) {
        View favTab = mViewPager.getChildAt(1);//Fav Tab
        if(favTab != null){
            RecyclerView rcyView = (RecyclerView)favTab.findViewById(R.id.recyclerView_favPlace);
            CardAdapter adapter = (CardAdapter)rcyView.getAdapter();

            Toast.makeText(this, "Removed from Favourites.",
                    Toast.LENGTH_LONG).show();
            if(adapter.removeCard(id)) {
                removeFromFile(id, getApplicationContext());
            }

        }
    }




    @Override
    public void openMapsFromLat(LatLng latlng) {
        Toast.makeText(this, ""+latlng.toString(),
                Toast.LENGTH_LONG).show();
    }


    public class AppSectionsPagerAdapter extends FragmentPagerAdapter {

        public AppSectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment tabFragment;
            switch (i) {
                case 0:
                     tabFragment = new PlacesFragment();
//                        Bundle arg1 = new Bundle();
//                        arg1.putIntArray(RightEarFragment.RIGHT_INDEX, buildIntArray(indexRight));
//                        rightFragment.setArguments(arg1);
                    return tabFragment;

                case 1:

                     tabFragment = new FavFragment();
//                        Bundle arg1 = new Bundle();
//                        arg1.putIntArray(RightEarFragment.RIGHT_INDEX, buildIntArray(indexRight));
//                        rightFragment.setArguments(arg1);
                    return tabFragment;

                default:
                     return  null;
            }

        }

        @Override
        public int getCount() {
            return  2;

        }

        @Override
        public CharSequence getPageTitle(int position) {
                    switch (position){
                        case 0: return "Places";
                        case 1: return "Favorites";
                        default: return "Section " + (position + 1);
                    }

        }
    }





}
