package green3rd.taxi.vber.tabFragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import green3rd.taxi.vber.R;
import green3rd.taxi.vber.sharedClass.CardAdapter;
import green3rd.taxi.vber.sharedClass.FileHandler;

import static green3rd.taxi.vber.sharedClass.FileHandler.readFromFile;


public class FavFragment extends Fragment {

    private OnFavSelectedListener mCallback;

    public FavFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // The last two arguments ensure LayoutParams are inflated properly.
        View rootView = inflater.inflate(
                R.layout.fragment_fav_tab, container, false);

        RecyclerView mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView_favPlace);
        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        //Read the favourite places on the file.
//        File file = new File(getContext().getFilesDir(), "vberFavPlaces.txt");




        ArrayList<String> nameArray,addressArray, urlArray,iconArray,idArray;
         ArrayList<LatLng> latLngArray;
        nameArray = new ArrayList<>();
        addressArray = new ArrayList<>();
        urlArray = new ArrayList<>();
        iconArray = new ArrayList<>();
        idArray = new ArrayList<>();
        latLngArray = new ArrayList<>();

        String jsonStr = readFromFile(FileHandler.filename, getContext());
//        Log.d("jsonStr","ReadFile :"+jsonStr);
        if (jsonStr != null && !jsonStr.equals("")) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                JSONArray resultsArray = jsonObj.getJSONArray("fav");
                String urlString = null;
                for (int i = 0; i < resultsArray.length(); i++) {
                    //Document
                    //https://developers.google.com/places/web-service/search?hl=th#PlaceSearchResponses
                    JSONObject dataObj = resultsArray.getJSONObject(i);
                    nameArray.add(dataObj.getString("name"));
                    addressArray.add(dataObj.getString("address"));
                    iconArray.add(dataObj.getString("icon").replace("\\", ""));
                    idArray.add( dataObj.getString("id")); //'id' contains a unique stable identifier denoting this place.

                    if(dataObj.has("url")) {
                        String urlStringTemp = dataObj.getString("url");
                        //https:\/\/maps.google.com\/maps\/contrib\/117315032338447460996\/photos
                            urlStringTemp = urlStringTemp.replace("\\","");//Remove '\'
                            urlString=urlStringTemp;
                    }
                    urlArray.add(urlString);

                    double placeLat = 13.7622871,placeLong = 100.5361068;//default
                    if(dataObj.has("lat") && dataObj.has("lng")) {
                        placeLat =  dataObj.getDouble("lat");
                        placeLong = dataObj.getDouble("lng");
                    }
                    latLngArray.add(
                            new LatLng(placeLat,placeLong)
                    );
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        RecyclerView.Adapter mAdapter = new CardAdapter(true,nameArray, addressArray, urlArray, iconArray,idArray,latLngArray,mCallback);
        mRecyclerView.setAdapter(mAdapter);


        return rootView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnFavSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement addToFavouriteTab");
        }
    }



}
