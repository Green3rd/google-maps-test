package green3rd.taxi.vber.tabFragment;


import android.app.Activity;
import android.app.ProgressDialog;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import green3rd.taxi.vber.sharedClass.CardAdapter;
import green3rd.taxi.vber.R;
import green3rd.taxi.vber.sharedClass.ServiceHandler;


public class PlacesFragment extends Fragment {

    private static final int REQUEST_PLACE_PICKER = 3822;//Random Number

    private RecyclerView mRecyclerView;

    private OnFavSelectedListener mCallback;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnFavSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement addToFavouriteTab");
        }
    }

    public PlacesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // The last two arguments ensure LayoutParams are inflated
        // properly.

        View rootView = inflater.inflate(
                R.layout.fragment_places_tab, container, false);
//        Bundle args = getArguments();
//        ((TextView) rootView.findViewById(android.R.id.text1)).setText(
//                Integer.toString(args.getInt(ARG_OBJECT)));
        Button selectCoButton =  (Button) rootView.findViewById(R.id.button_selectCoordinates);
        selectCoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), MapsActivity.class);
//                startActivity(intent);

                //Start the Picker Activity (Google Places API)
                try {
                    //TODO
                    // Can't custom the marker? Must implement it myself?
                    //http://stackoverflow.com/questions/31958776/google-place-picker-api-query-for-app-scoped-places/32065508#32065508
                    //http://googlegeodevelopers.blogspot.com/2015/06/code-road-android-app-checking-in-using.html



                    final LocationManager manager = (LocationManager) getContext().getSystemService( Context.LOCATION_SERVICE );

                    PlacePicker.IntentBuilder intentBuilder;
                    if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
//                        Log.d("GPS","Not open GPS.");
                        LatLngBounds lat = new LatLngBounds( //Default location
                                new LatLng(13.7622871,100.5361068),//top left
                                new LatLng(13.764934,100.5377483));// bottom right

                        intentBuilder =
                                new PlacePicker.IntentBuilder()
                                        .setLatLngBounds(lat)
                                ;
                    }else{
                         intentBuilder = new PlacePicker.IntentBuilder();
                    }



                    Intent intent = intentBuilder.build(getActivity());
                    // Start the intent by requesting a result,
                    // identified by a request code.

                    startActivityForResult(intent, REQUEST_PLACE_PICKER);
//                    new SearchPlaces().execute();
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }

            }
        });

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView_placeNearby);
        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);


        return rootView;

    }


    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode, Intent data) {

        if (requestCode == REQUEST_PLACE_PICKER
                && resultCode == Activity.RESULT_OK) {

            // The user has selected a place. Extract the name and address.
            Place place = PlacePicker.getPlace(getActivity(),data);

            if(getView()!=null) {
                Button selectCoButton = (Button) getView().findViewById(R.id.button_selectCoordinates);
                selectCoButton.setText(place.getName());
            }

            new SearchPlaces(place.getLatLng()).execute();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }


    }



    /**
     * Async task class to get json by making HTTP call
     * */
    private class SearchPlaces extends AsyncTask<Void, Void, Void> {
        private ProgressDialog pDialog;
        private boolean isDownloadSuccess=false;
        private final LatLng pickedLocation;

//        private String[] nameArray,addressArray, urlArray,iconArray;
        private ArrayList<String> nameArray,addressArray, urlArray,iconArray,idArray;
        private ArrayList<LatLng> latLngArray;
        public SearchPlaces(LatLng pickedLocation) {
            this.pickedLocation = pickedLocation;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();


            // Making a request to url and getting response
            String url = getString(R.string.url_place_search)+
                    "location="+ pickedLocation.latitude+","+pickedLocation.longitude+
                    "&radius=" + 3000+//3 kilometers
//                    "&types=food"+
//                    "&name=cruise"+
                    "&key="+getString(R.string.google_maps_key_web_service);

//            ContentValues values=new ContentValues();
//            values.put("menuID",menuID);
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);
//            Log.d("JSON ", "Google url > " + url);
//            Log.d("Response: ", "Google Response > " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node from the Object
                    JSONArray resultsArray = jsonObj.getJSONArray("results");
//                    Log.d("Json","VBer resultsArray "+resultsArray.toString());


                    nameArray = new ArrayList<>();
                    addressArray = new ArrayList<>();
                    urlArray = new ArrayList<>();
                    iconArray = new ArrayList<>();
                    idArray = new ArrayList<>();
                    latLngArray = new ArrayList<>();

                    String urlString = null;
                    for (int i = 0; i < resultsArray.length(); i++) {
                        //Document
                        //https://developers.google.com/places/web-service/search?hl=th#PlaceSearchResponses
                        JSONObject dataObj = resultsArray.getJSONObject(i);
                        nameArray.add(dataObj.getString("name"));
                        addressArray.add(dataObj.getString("vicinity"));
                        iconArray.add(dataObj.getString("icon"));
                        idArray.add( dataObj.getString("id")); //'id' contains a unique stable identifier denoting this place.

                        if(dataObj.has("photos")) {
                            JSONObject photoObj = (JSONObject) (dataObj.getJSONArray("photos").get(0));
                             String urlStringTemp = photoObj.getString("html_attributions");
                            // Now urlString = "<a href=\"https://maps.google.com/maps/contrib/101278300131345086916/photos\"
                            int indexFront = urlStringTemp.indexOf("https:");
                            String backWord = "/photos";
                            int indexBack = urlStringTemp.indexOf(backWord);
                            if(indexFront > 0 && indexBack > 0){
                                urlStringTemp=urlStringTemp.substring(indexFront, indexBack + backWord.length());
                                //https:\/\/maps.google.com\/maps\/contrib\/117315032338447460996\/photos
                                urlStringTemp = urlStringTemp.replace("\\","");//Remove '\'
                                urlString=urlStringTemp;
                            }
                        }
                        urlArray.add(urlString);
//                        Log.d("Json", "VBer resultsArray " + i + ", name:" + nameArray[i] + " Address:" + addressArray[i]+" url:"+urlString);

                        double placeLat = 13.7622871,placeLong = 100.5361068;//default
                        if(dataObj.has("geometry")) {
                            JSONObject locationObj = (dataObj.getJSONObject("geometry").getJSONObject("location"));
                            placeLat =  locationObj.getDouble("lat");
                            placeLong = locationObj.getDouble("lng");
                        }
                        latLngArray.add(
                                new LatLng(placeLat,placeLong)
                        );
                    }
                    isDownloadSuccess=true;
                } catch (JSONException e) {
                    e.printStackTrace();
                    isDownloadSuccess=false;
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
                isDownloadSuccess=false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into RecyclerView
             * */
            if(isDownloadSuccess) {
                // specify an adapter (see also next example)
                RecyclerView.Adapter mAdapter = new CardAdapter(false,nameArray, addressArray, urlArray, iconArray,idArray,latLngArray,mCallback);
                mRecyclerView.setAdapter(mAdapter);

                    }

                    else

                    {
                        Toast.makeText(getActivity(), "Download places failed. Please try again.",
                                Toast.LENGTH_LONG).show();
                    }

                }


            }



}
