package green3rd.taxi.vber.tabFragment;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Akamu on 1/31/2016 AD.
 * To
 */
// Container Activity must implement this interface
public interface OnFavSelectedListener {
    void addToFavouriteTab(String name, String address, String url, String icon, String id,LatLng latLng);
    void removeFromFavouriteTab(String id);
    void openMapsFromLat(LatLng latlng);
}
