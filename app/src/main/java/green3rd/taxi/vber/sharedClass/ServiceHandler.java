package green3rd.taxi.vber.sharedClass;

import android.content.ContentValues;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.Set;

/**
 * Created by Akamu on 6/13/15 AD.
 * Handle the HTTP Service
 */
public class ServiceHandler {

    private static String response = null;
    public final static int GET = 1;
    private final static int POST = 2;

    public ServiceHandler() {

    }

    /**
     * Making service call
     * @param url - url to make request
     * @param method - http request method
     * */
    public String makeServiceCall(String url, int method) {
        return this.makeServiceCall(url, method, null);
    }



    private final OkHttpClient client = new OkHttpClient();
    private String connectHTTP(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }



    private String postHTTP(String url, ContentValues contentValue) throws IOException {

        FormEncodingBuilder formEncode = new FormEncodingBuilder();
        Request request;
        if(contentValue!=null) {
            Set<String> keySet = contentValue.keySet();

          //  Log.d("postHTTP","postHTTP "+keySet.size()+",contentValue "+contentValue.size());
            for (String key : keySet) {
                formEncode.add(key,contentValue.getAsString(key));
            //    Log.d("postHTTP", "postHTTP key" + key + ",value " +contentValue.getAsString(key));
            }

            RequestBody formBody = formEncode.build();
             request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();
        }else{
             request = new Request.Builder()
                    .url(url)
                    .build();
        }


        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    /**
     * Making service call
     * @param urlInput - url to make request
     * @param method - http request method
     * @param contentValue - http request params
     * */
    private String makeServiceCall(String urlInput, int method,
                                   ContentValues contentValue) {


        try {

            // Checking http request method type
            if (method == POST && contentValue!=null) {

                response=postHTTP(urlInput, contentValue);

            } else if (method == GET) {

                response= connectHTTP(urlInput);
            }
//            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
//            response = getStringFromInputStream(in);

//            if(httpResponse!=null)httpEntity = httpResponse.getEntity();
//            response = EntityUtils.toString(httpEntity);

        } catch (IOException e) {
            e.printStackTrace();
        }
// finally {
//            if(urlConnection!=null)
//            urlConnection.disconnect();
//        }

        return response;

    }


//
//    private static String getStringFromInputStream(InputStream is) {
//
//        BufferedReader br = null;
//        StringBuilder sb = new StringBuilder();
//
//        String line;
//        try {
//
//            br = new BufferedReader(new InputStreamReader(is));
//            while ((line = br.readLine()) != null) {
//                sb.append(line);
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (br != null) {
//                try {
//                    br.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        return sb.toString();
//
//    }

}
