package green3rd.taxi.vber.sharedClass;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import green3rd.taxi.vber.R;
import green3rd.taxi.vber.tabFragment.OnFavSelectedListener;

/**
 * Created by Akamu on 1/29/2016 AD.
 * To
 */
public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {
//    private String[] nameArray,addressArray, urlArray,iconArray;
    private final ArrayList<String> nameArray;
    private final ArrayList<String> addressArray;
    private final ArrayList<String> urlArray;
    private final ArrayList<String> iconArray;
    private final ArrayList<String> idArray;
    private final boolean[] isFavArray;
    private final ArrayList<LatLng> latLngArray;
    private final OnFavSelectedListener callbackToActivity;
    private boolean isFromFavFragment=false;
    public boolean addCard(String name, String address, String url, String icon,String id,LatLng latLng) {

        for(int i=0;i < idArray.size();i++){
            if(idArray.get(i).equals(id)){
                return false;//already have
            }
        }

        nameArray.add(name);
        addressArray.add(address);
        urlArray.add(url);
        iconArray.add(icon);
        idArray.add(id);
        latLngArray.add(latLng);
        notifyItemInserted(nameArray.size() - 1);
        return true;
    }

    public boolean removeCard(String id) {
        //Search
        int position=-1;
        for(int i=0;i < idArray.size();i++){
            if(idArray.get(i).equals(id)){
                position = i;
                break;
            }
        }

        if(position >= 0) {
            nameArray.remove(position);
            addressArray.remove(position);
            urlArray.remove(position);
            iconArray.remove(position);
            idArray.remove(position);
            latLngArray.remove(position);
            notifyItemRemoved(position);


            return true;
        }
        return false;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {
        // each data item is just a string in this case
        private final TextView mTextView_name;
        private final TextView mTextView_address;
        private final TextView mTextView_url;
        private final ImageButton favButton;
        private final IMyViewHolderClicks mListener;
        private final ImageView placePicture;


        public ViewHolder(View itemView,IMyViewHolderClicks listener) {
            super(itemView);
            mListener = listener;
            itemView.setOnClickListener(this);
            mTextView_name = (TextView) itemView.findViewById(R.id.card_name);
            mTextView_address = (TextView) itemView.findViewById(R.id.card_address);
            mTextView_url = (TextView) itemView.findViewById(R.id.card_url);
            favButton = (ImageButton) itemView.findViewById(R.id.imageButton_fav);
            favButton.setOnClickListener(this);
            placePicture = (ImageView) itemView.findViewById(R.id.imageView_Icon);
        }

        @Override
        public void onClick(View v) {
            if (v instanceof ImageButton){

                if(isFromFavFragment){
                    //In FavFragment
                    //Un-favourite
                    mListener.removeFromFavourite(getAdapterPosition());
                }
                else if(!isFavArray[getAdapterPosition()]) {
                    //Change icon of the fav button.
                    this.favButton.setImageResource(R.drawable.image_button_fav_selected);
                    mListener.addToFavourite(getAdapterPosition());
                    isFavArray[getAdapterPosition()] = true;
                }else{
                    //Un-favourite
                    this.favButton.setImageResource(R.drawable.image_button_fav);
                    mListener.removeFromFavourite(getAdapterPosition());
                    isFavArray[getAdapterPosition()] = false;
                }
            } else {
                mListener.openMaps(getAdapterPosition());
            }
//            Log.d("CardAdapter", "onClick " + getAdapterPosition());
        }
    }

    public  interface IMyViewHolderClicks {
        void openMaps(int adapterPosition);
        void addToFavourite(int adapterPosition);
        void removeFromFavourite(int adapterPosition);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
//    public CardAdapter(boolean isFromFavFragment, ArrayList<String> nameArray, ArrayList<String> addressArray, ArrayList<String> urlArray, ArrayList<String> iconArray, ArrayList<String> idArray, ArrayList<LatLng> latLngArray, OnFavSelectedListener mCallback) {
//        this(isFromFavFragment,nameArray, addressArray, urlArray, iconArray,idArray,latLngArray, null);
//    }

    public CardAdapter(boolean isFromFavFragment, ArrayList<String> nameArray, ArrayList<String> addressArray, ArrayList<String> urlArray, ArrayList<String> iconArray, ArrayList<String> idArray, ArrayList<LatLng> latLngArray,OnFavSelectedListener mCallback) {
        this.nameArray = nameArray;
        this.addressArray = addressArray;
        this.urlArray = urlArray;
        this.iconArray = iconArray;
        this.idArray = idArray;
        this.callbackToActivity = mCallback;
        isFavArray = new boolean[nameArray.size()];
        for(int i=0;i<isFavArray.length;i++){
            isFavArray[i] = isFromFavFragment;
        }
        this.latLngArray = latLngArray;
        this.isFromFavFragment = isFromFavFragment;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
//        v.setOnClickListener(mOnClickListener);

        IMyViewHolderClicks vhClick = new IMyViewHolderClicks(){
            @Override
            public void openMaps(int adapterPosition) {
//                Log.d("CardAdapter", "onClick openMaps");
                if(callbackToActivity!=null) {
                    callbackToActivity.openMapsFromLat(latLngArray.get(adapterPosition));
                }
            }

            @Override
            public void addToFavourite(int adapterPosition) {
              if(callbackToActivity!=null &&isFavArray.length>0 ) {
                  callbackToActivity.addToFavouriteTab(nameArray.get(adapterPosition),
                          addressArray.get(adapterPosition), urlArray.get(adapterPosition),
                          iconArray.get(adapterPosition), idArray.get(adapterPosition),
                          latLngArray.get(adapterPosition)
                  );
              }
            }

            @Override
            public void removeFromFavourite(int adapterPosition) {
                if(callbackToActivity!=null) {
                    callbackToActivity.removeFromFavouriteTab(idArray.get(adapterPosition));
                }
            }
        };
        return new ViewHolder(v,vhClick);
    }




    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTextView_name.setText(Html.fromHtml("<b>" + "Name: " + "</b>"+nameArray.get(position)));
        holder.mTextView_address.setText(Html.fromHtml("<b>" + "Address: " + "</b>"+addressArray.get(position)));
        if(urlArray.get(position)!=null) {
            holder.mTextView_url.setText(Html.fromHtml("<b>" + "URL Link: " + "</b>" + urlArray.get(position)));
        }else{
            holder.mTextView_url.setText("");
        }

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(iconArray.get(position), holder.placePicture);


        if(isFavArray!=null && !isFromFavFragment && !isFavArray[position]) {
            holder.favButton.setImageResource(R.drawable.image_button_fav);
        }else{
            //favourite
            holder.favButton.setImageResource(R.drawable.image_button_fav_selected);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return nameArray.size();
    }
}