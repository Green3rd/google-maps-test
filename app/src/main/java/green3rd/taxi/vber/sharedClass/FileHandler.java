package green3rd.taxi.vber.sharedClass;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Akamu on 1/31/2016 AD.
 * To
 */
public class FileHandler {

    public static final String filename="vber.txt";

    public static void writeToFile(String name, String address, String url, String icon, String id, LatLng latlng, Context context) {

//        context.deleteFile(filename);


        JSONObject innerObject;// New object
        JSONObject outerObject;//Result Onject
        try {
            innerObject = new JSONObject();
            innerObject.put("name", name);
            innerObject.put("address", address);
            if (url != null) innerObject.put("url", url);
            innerObject.put("icon", icon);
            innerObject.put("id", id);
            innerObject.put("lat", latlng.latitude);
            innerObject.put("lng", latlng.longitude);


        //Check if the file empty
        String formerText = readFromFile(filename, context);
        if (formerText != null && !formerText.equals("")) {

                JSONObject jsonObj = new JSONObject(formerText);
                JSONArray resultsArray = jsonObj.getJSONArray("fav");
                resultsArray.put(innerObject);
            outerObject = new JSONObject();
            outerObject.put("fav", resultsArray);


        }else {
            //New File
            JSONArray dataArray = new JSONArray();
             outerObject = new JSONObject();
            dataArray.put(innerObject);
            outerObject.put("fav", dataArray);

            /*
            file {
                    "fav":[
                    {
                        "id":"a958c62a3601130ef2dcadb0f126d1b825f67380",
                        "icon":"https:\/\/maps.gstatic.com\/mapfiles\/place_api\/icons\/bar-71.png",
                        "lng":100.553116,
                        "address":"Thanon Phetchaburi, Sukhum Vit",
                        "lat":13.741532,
                        "url":"https:\/\/maps.google.com\/maps\/contrib\/105301064330354038290\/photos",
                        "name":"Nana Nightclub VIP Party Tours"
                    }
                    ]
                }
             */


    //        Log.d("writefile","rewrite file "+outerObject.toString());


        }


            try {
                FileOutputStream outputStream;
                outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
                outputStream.write(outerObject.toString().getBytes());
                outputStream.close();

            }
            catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static void removeFromFile(String id, Context context) {


        //Check if the file empty
        String formerText = readFromFile(filename, context);
        if (formerText != null) {


            try {
                JSONObject jsonObj = new JSONObject(formerText);
                JSONArray resultsArray = jsonObj.getJSONArray("fav");
                int targetedPosition=-1;
                for (int i = resultsArray.length()-1; i >= 0 ; --i) {
                    JSONObject dataObj = resultsArray.getJSONObject(i);
                    if (dataObj.getString("id").equals(id)){
                        targetedPosition = i;
                        break;
                    }
                }
                if(targetedPosition>=0){
                    resultsArray.remove(targetedPosition);
                    JSONObject outerObject = new JSONObject();
                    outerObject.put("fav", resultsArray);

                    try {
                        FileOutputStream outputStream;
                        outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
                        outputStream.write(outerObject.toString().getBytes());
                        outputStream.close();

                    }
                    catch (IOException e) {
                        Log.e("Exception", "File write failed: " + e.toString());
                    }


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }



    }

    public static String readFromFile(String filename, Context context) {

//        Log.d("readFromFile", "[Cache] load from Cache: " + filename);
        ByteArrayOutputStream stream = new ByteArrayOutputStream ();
        try {
            InputStream inputStream = context.openFileInput(filename);

            if ( inputStream != null ) {
                byte[] buffer = new byte[8064];
                while(true) {
                    int rd = inputStream.read(buffer);
                    if(rd == -1)break;
                    stream.write(buffer, 0, rd);
                }

                stream.flush();
                inputStream.close();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("FileHandler", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("FileHandler", "Can not read file: " + e.toString());
        }

        return stream.toString();
    }

}
